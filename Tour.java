/*
   Tour.java
   Models a tour of Cities
*/
import java.util.ArrayList;
import java.util.Collections;

public class Tour{
   private final int size;
   ArrayList<City> list = new ArrayList<City>();

   public Tour(int n){
      size = n;
      for(int i = 0; i < size; i++){
         list.add(new City());
      }
   }

   public Tour(ArrayList<City> alist){
      size = alist.size();
      for(int i = 0; i < size; i++){
         list.add(alist.get(i).clone());
      }
   }
   
   public int size(){
      return size;
   }
   
   public double distance(){
      double d=0;
      for(int i=1; i<size;i++) 
         d+=City.distance(list.get(i-1),list.get(i));
      d+=City.distance(list.get(size-1),list.get(0));
      return d;
   }

/*
   Swap 2 Cities
*/
   public void swap(int c1,int c2){
      City tc = list.get(c1);
      list.set(c1,list.get(c2));
      list.set(c2,tc);
   }

   public void shuffle(){
      Collections.shuffle(list);
   }

   public Tour clone(){
      Tour newTour=new Tour(list); 
      return newTour;
   }

   public Tour neighbour(){
      Tour next=this.clone();
      int c1=(int)(Math.random()*size);
      int c2=(int)(Math.random()*size);
      next.swap(c1,c2);
      return next;
   }

   public String toString(){
      String s=new String();
      for(int i=0; i<size; i++)
         s+=list.get(i).toString()+" --> ";
      return s;
   }

   public static void main(String args[]){
      System.out.println("Randomly generating new "+args[0]+" size tour...");
      Tour t=new Tour(Integer.parseInt(args[0]));
      System.out.println(t);
      System.out.println("Tot Distance: "+t.distance());
      System.out.println("Diagonally generating new "+args[0]+" size tour...");
      ArrayList<City> list = new ArrayList<>();
      for(int i = 0; i < Integer.parseInt(args[0]); i++)
   		list.add(new City(i,i));
		t = new Tour(list);
      System.out.println(t);
      System.out.println("Tot Distance: "+t.distance());

   }
}
