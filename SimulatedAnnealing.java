/*
   Simulated annealing with abstract methods
*/

import java.util.ArrayList;
import java.util.Collections;

public class SimulatedAnnealing{
   private double cRate=0.001;   
   private double temp=20000;
   private double e1,e2;
   private Tour currSol,newSol,best;
   private Tour testTour;//Input Tour
   
   public SimulatedAnnealing(Tour t){
      testTour=t;
   }

   public void setCRate(double r){
      if(r<=0 || r>=1) 
         throw new IllegalArgumentException();
      cRate=r;   
   }

   public void setTemp(double t){
      if(t<=0) 
         throw new IllegalArgumentException();
      temp=t;   
   }
/*
   accFunc() and cool() can be modified by subclasses
*/
   protected double accFunc(){
      if(e2<e1)
         return 1.0;
      return Math.exp((e1-e2)/temp);
   }

   protected void cool(){
      temp=temp*(1-cRate);
   }

   public void anneal(){
      currSol=testTour.clone();
      currSol.shuffle();
      best=currSol.clone();
      double sd = currSol.distance();
      System.out.println(currSol);
      while(temp>1){
         newSol=currSol.neighbour();
         e1 = currSol.distance(); 
         e2 = newSol.distance();
         if(accFunc() > Math.random()){
            currSol=newSol.clone();
            if(e1 < e2){
//            	System.out.println("Worse solution accepted: " + currSol.distance() + " (e1 = "+e1+" e2 = "+e2+" temp = "+temp+")");
            	System.out.format("Worse solution accepted: %7.1f \t (e1 = %7.1f \t e2 = %7.1f \t temp = %8.1f)\n", 
            		currSol.distance(), e1, e2, temp);
          	}
         }
         if(currSol.distance()<best.distance()){
            best=currSol.clone();
            System.out.format("!!! New minimum: %7.1f\n",best.distance());            
            }
         cool();   
      }

      System.out.format("\n### Initial random solution: %7.1f\n### Final solution: %7.1f\n", sd, best.distance());
   }
   
   public Tour getBest(){
   	return best;
   }

   public static void main(String[] args){
      System.out.println("Usage: java SimulatedAnnealing <numberOfCity> <temp> <coolingRate>");      
      if(args.length <= 2) 
      	System.exit(1);
      System.out.println("Generating "+Integer.parseInt(args[0])+" aligned cities...");
   	ArrayList<City> list = new ArrayList<>();
   	for(int i = 0; i < Integer.parseInt(args[0]); i++)
   		list.add(new City(i,i));
   	Collections.shuffle(list);
  	   Tour t = new Tour(list);	   	   		
  	   
      Timer timer=new Timer();
      SimulatedAnnealing test=new SimulatedAnnealing(t);
      test.setTemp(Integer.parseInt(args[1]));
      test.setCRate(Double.parseDouble(args[2]));
      timer.start();
      test.anneal();
      timer.stop();
      System.out.println("Terminated in "+timer.read()+"ms");
		double d = 2 * Math.sqrt(2) * (Integer.parseInt(args[0]) - 1);   	
		System.out.format("\n### Optimum solution: %7.1f\n",d);   	   	
		System.out.format("### Ratio: %3.2f\n", d / test.getBest().distance());
   }
}
