# README #

This code implements a simple Simulated Annealing metaheuristic algorithm.

### What is this repository for? ###

* Simple Simulated Annealing implementation
* version 0.9

### How do I get set up? ###

Compile with Java OpenJDK 7: javac SimulatedAnnealing.java

### Contribution guidelines ###

### Who do I talk to? ###

Author: Enrico R.
