/*
   City.java
   Models a city
*/

public class City{
   private final int x,y;
   private static final int maxX=1000, maxY=1000;
   
   public City(int ax, int ay){ //TODO Manage xy outOfBoundException
      x=ax; y=ay;
   }

   public City(){      
      this((int) (Math.random()*maxX), (int) (Math.random()*maxY));
   }

   public int getX(){
      return x;
   }

   public int getY(){
      return y;
   }

   public City clone(){
      City newCity = new City(x,y);
      return newCity;
   }

   public static double distance(City c1, City c2){
      int dx=c1.getX()-c2.getX();
      int dy=c1.getY()-c2.getY();
      return Math.sqrt(dx*dx + dy*dy);
   }

   public String toString(){
      return ("("+getX()+","+getY()+")");
   }

   public static void main(String[] args){
      System.out.println("New city: ");
      City c1=new City();
      System.out.println(c1);
   }
}
