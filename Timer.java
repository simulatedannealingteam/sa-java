/*
   Timer.java
   Simple Timer
*/

public class Timer{
   private double tstart, tstop, telap;

   public Timer(){}

   public void start(){
      tstart=System.currentTimeMillis();
   }

   public void stop(){
      tstop=System.currentTimeMillis(); 
      telap=tstop-tstart;
   }

   public double read(){
      return telap;
   }

   public void reset(){
      tstop=0;
      tstart=0;
   }

   public static void main(String args[]) throws InterruptedException{
      System.out.println("Starting timer");
      Timer t=new Timer();
      t.start();
      Thread.sleep(3000);
      t.stop();
      System.out.println("Passed "+t.read()+"ms");
   }
}
